package com.directions.Team42;

import android.Manifest;
import android.content.Context;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;


/*
 * Portions (c) 2009 Google, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Coby Plain coby.plain@gmail.com, Ali Muzaffar ali@muzaffar.me
 */

public class Compass extends FragmentActivity {



	private static final String TAG = "Compass";
	private static boolean DEBUG = true;
	private SensorManager mSensorManager;
	public static Location myLastLocation;
	private Sensor mSensor;
	private DrawView mDrawView;
	LocationManager locMgr;
	LocationListener locListener;



	private final SensorEventListener mListener = new SensorEventListener() {
		private float[] mRotationMatrix = new float[16];
		private float[] mValues = new float[3];

		public void onSensorChanged(SensorEvent event) {
			SensorManager.getRotationMatrixFromVector(mRotationMatrix, event.values);
			SensorManager.getOrientation(mRotationMatrix, mValues);

			if (DEBUG)
				Log.d(TAG, "sensorChanged (" + Math.toDegrees(mValues[0]) + ", " + Math.toDegrees(mValues[1]) + ", " + Math.toDegrees(mValues[2]) + ")");

			if (mDrawView != null) {
				mDrawView.setOffset((float) Math.toDegrees(mValues[0]));
				mDrawView.invalidate();
			}
		}

		public void onAccuracyChanged(Sensor sensor, int accuracy) {
		}
	};


	@Override
	protected void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
		setContentView(R.layout.camera);

		mDrawView = (DrawView) findViewById(R.id.drawSurfaceView);
		ImageView arr =(ImageView)findViewById(R.id.arrow);
		arr.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent(getApplicationContext(),MainActivity.class);
				startActivity(intent);

			}
		});




		locMgr = (LocationManager) this.getSystemService(LOCATION_SERVICE); // <2>
		LocationProvider high = locMgr.getProvider(locMgr.getBestProvider(
				LocationUtils.createFineCriteria(), true));

		if (Build.VERSION.SDK_INT >= 23 &&
				ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
				ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			return;
		}
		locMgr.requestLocationUpdates(high.getName(), 0, 0f,
				locListener = new LocationListener() {
					public void onLocationChanged(Location location) {
						// do something here to save this new location
						Log.d(TAG, "Location Changed");
						mDrawView.setMyLocation(location.getLatitude(), location.getLongitude());
						Log.d("val", String.valueOf(mDrawView.me.latitude));
						mDrawView.invalidate();
						myLastLocation = location;
						//	mMap.addMarker(new MarkerOptions().position(new LatLng(mDrawView.me.latitude,mDrawView.me.longitude)).title("Marker"));

					}

					public void onStatusChanged(String s, int i, Bundle bundle) {

					}

					public void onProviderEnabled(String s) {
						// try switching to a different provider
					}

					public void onProviderDisabled(String s) {
						// try switching to a different provider
					}
				});

	}


	@Override
	protected void onResume() {
		if (DEBUG)
			Log.d(TAG, "onResume");
		super.onResume();


		mSensorManager.registerListener(mListener, mSensor,
				SensorManager.SENSOR_DELAY_GAME);
	}

	@Override
	protected void onStop() {
		if (DEBUG)
			Log.d(TAG, "onStop");
		mSensorManager.unregisterListener(mListener);
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		if ( Build.VERSION.SDK_INT >= 23 &&
				ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
				ContextCompat.checkSelfPermission( getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			return  ;
		}
		locMgr.removeUpdates(locListener);
		super.onDestroy();
	}

	private void setUpMap() {

	}



}
