package com.directions.Team42;

/**
 * Created by Mahesh on 3/12/2016.
 */

public class Point {
    public double longitude = 0f;
    public double latitude = 0f;
    public double altitude = 0f;
    public String description;
    public float x, y = 0;

    public Point(double lat, double lon, String desc) {
        this.latitude = lat;
        this.longitude = lon;

        this.description = desc;
    }
}

